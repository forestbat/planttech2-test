package net.kaneka.planttech2.gui;

public class GUIReferences
{
	public static final String GUI_MEGA_FURNACE = "tileentitymegafurnace"; 
	public static final String GUI_IDENTIFIER = "tileentityidentifier"; 
	public static final String GUI_SEEDSQUEEZER = "tileentityseedsqueezer";
	public static final String GUI_SOLARGENERATOR = "tileentitysolargenerator";
	public static final String GUI_PLANTFARM = "tileentityplantfarm"; 
	public static final String GUI_DNA_EXTRACTOR = "tileentitydnaextractor"; 
	public static final String GUI_DNA_REMOVER = "tileentitydnaremover"; 
	public static final String GUI_DNA_COMBINER = "tileentitydnacombiner"; 
	public static final String GUI_SEEDCONSTRUCTOR = "tileentityseedconstructor"; 
	public static final String GUI_DNA_CLEANER = "tileentitydnacleaner"; 
	public static final String GUI_COMPRESSOR = "tileentitycompressor"; 
	public static final String GUI_ENERGYSTORAGE = "tileentityenergystorage";
	public static final String GUI_INFUSER = "tileentityinfuser";
	public static final String GUI_ITEMUPGRADEABLE = "itemupgradeable"; 
}
