package net.kaneka.planttech2.items.upgradeable;

public class ItemUpgradeableHand extends ItemBaseUpgradeable
{

	public ItemUpgradeableHand(String name, Properties property, int basecapacity, int maxInvSize, float baseAttack, float baseAttackSpeed)
	{
		super(name, property, basecapacity, maxInvSize, baseAttack, baseAttackSpeed);
	}

}
